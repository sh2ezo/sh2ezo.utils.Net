﻿This is my implementation of the managed network library for using in some my projects.

Now it supports:

* HTTP
* HTTPS
* Cookies
* SOCKS5 Proxy

Usage:

```csharp
    var proxyAddress = IPAddress.Parse("XXX.XXX.XXX.XXX");
    var proxyEndPoint = new IPEndPoint(proxyAddress, 12345);
    var factory = new Socks5ConnectionFactory(
        "username",
        "password",
        proxyEndPoint
    );
    var handler = new ClientHandler(factory);
    var client = new HttpClient(handler);
    var googleMainPage = client.GetStringAsync("https://google.com");
    Console.WriteLine(googleMainPage);
```

This library may contain (I think it contains) some (critical or not) bugs.