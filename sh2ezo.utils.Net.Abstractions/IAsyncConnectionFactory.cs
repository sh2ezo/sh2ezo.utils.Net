﻿using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace sh2ezo.utils.Net.Abstractions
{
    public interface IAsyncConnectionFactory
    {
        Task<NetworkStream> ConnectAsync(EndPoint endPoint, CancellationToken cancellationToken);
    }
}
