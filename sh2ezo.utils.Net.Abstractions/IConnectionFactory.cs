﻿using System.Net;
using System.Net.Sockets;

namespace sh2ezo.utils.Net.Abstractions
{
    public interface IConnectionFactory
    {
        NetworkStream Connect(EndPoint endPoint);
    }
}
