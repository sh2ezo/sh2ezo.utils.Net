﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh2ezo.utils.Net.Socks5
{
    internal class ReceiveBuffer
    {
        public byte[] Bytes { get; }
        public int Offset;

        public ReceiveBuffer(int size)
        {
            Bytes = new byte[size];
            Offset = 0;
        }
    }
}
