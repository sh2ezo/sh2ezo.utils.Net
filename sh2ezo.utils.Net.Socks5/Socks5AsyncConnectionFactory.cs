﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using sh2ezo.utils.Net.Abstractions;

namespace sh2ezo.utils.Net.Socks5
{
    public class Socks5AsyncConnectionFactory : IAsyncConnectionFactory
    {
        private readonly IPAddress _host;
        private readonly int _port;
        private string _username;
        private string _password;
        public TimeSpan Timeout { get; set; }
        private readonly byte[] _helloMessage;

        private const int AuthMethodNotRequired = 0;
        private const int AuthMethodUsernamePassword = 2;

        public Socks5AsyncConnectionFactory(IPAddress host, int port, string username, string password)
        {
            _host = host;
            _port = port;
            _username = username;
            _password = password;
            
            Timeout = new TimeSpan(0, 0, 30);

            _helloMessage = SocksMessageBuilder.GetHelloMessage(methods: new byte[]
            {
                AuthMethodNotRequired,
                AuthMethodUsernamePassword
            });
        }

        public async Task<NetworkStream> ConnectAsync(EndPoint endPoint, CancellationToken cancellationToken)
        {
            // connect to proxy
            var socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            await socket.ConnectAsync(_host, _port).ConfigureAwait(false);
            
            var stream = new NetworkStream(socket, true);

            try
            {
                // authenticate
                await AuthenticateAsync(stream, cancellationToken).ConfigureAwait(false);

                // connect to needed target
                var connectMessage = SocksMessageBuilder.GetConnectMessage(endPoint);
                await stream.WriteAsync(connectMessage, 0, connectMessage.Length, cancellationToken).ConfigureAwait(false);

                // receive answer
                var reply = await ReceiveCommandReplyAsync(stream, cancellationToken).ConfigureAwait(false);

                // check status
                if (reply.ReplyType != SocksReplyType.Success)
                {
                    throw new SocksNotConnectedException(reply.ReplyType);
                }

                return stream;
            }
            catch
            {
                stream.Close();
                stream.Dispose();
                throw;
            }
        }

        private async Task<SocksReply> ReceiveCommandReplyAsync(NetworkStream stream, CancellationToken cancellationToken)
        {
            var replyBuffer = new ReceiveBuffer(32);

            while (replyBuffer.Offset < 2)
            {
                await ReceiveBytesAsync(stream, replyBuffer, cancellationToken).ConfigureAwait(false);
            }

            // parse header
            var reply = new SocksReply
            {
                Version = replyBuffer.Bytes[0],
                ReplyType = (SocksReplyType)replyBuffer.Bytes[1]
            };

            // check status
            if (reply.ReplyType != SocksReplyType.Success)
            {
                return reply;
            }

            while(replyBuffer.Offset < 4)
            {
                await ReceiveBytesAsync(stream, replyBuffer, cancellationToken).ConfigureAwait(false);
            }

            int addressType = replyBuffer.Bytes[3];

            // parse address
            if (addressType == 1) // IPv4
            {
                while (replyBuffer.Offset < 4 + 4 + 2) // header + IPv4 + port
                {
                    await ReceiveBytesAsync(stream, replyBuffer, cancellationToken).ConfigureAwait(false);
                }

                var ipBytes = new byte[4];
                Array.Copy(replyBuffer.Bytes, 4, ipBytes, 0, 4);

                var port = (replyBuffer.Bytes[9] << 8) | replyBuffer.Bytes[8];

                reply.BoundEndPoint = new IPEndPoint(new IPAddress(ipBytes), port);
            }
            else if (addressType == 2) // DNS
            {
                while (replyBuffer.Offset < 4 + 1) // header + host name length
                {
                    await ReceiveBytesAsync(stream, replyBuffer, cancellationToken).ConfigureAwait(false);
                }

                var hostNameLength = replyBuffer.Bytes[4];

                while (replyBuffer.Offset < 4 + 1 + hostNameLength) // header + host name length + host name
                {
                    await ReceiveBytesAsync(stream, replyBuffer, cancellationToken).ConfigureAwait(false);
                }

                var host = Encoding.ASCII.GetString(replyBuffer.Bytes, 5, hostNameLength);
                int port = (replyBuffer.Bytes[4 + host.Length + 1] << 8) | replyBuffer.Bytes[4 + host.Length];

                reply.BoundEndPoint = new DnsEndPoint(host, port);
            }
            else if (addressType == 4) // IPv6
            {
                while (replyBuffer.Offset < 4 + 1 + 16 + 2) // header + IPv6 + port
                {
                    await ReceiveBytesAsync(stream, replyBuffer, cancellationToken).ConfigureAwait(false);
                }

                var ipBytes = new byte[16];
                Array.Copy(replyBuffer.Bytes, 4, ipBytes, 0, 16);

                var port = (replyBuffer.Bytes[21] << 8) | replyBuffer.Bytes[20];

                reply.BoundEndPoint = new IPEndPoint(new IPAddress(ipBytes), port);
            }
            else
            {
                throw new ArgumentException("Wrong address type");
            }

            return reply;
        }

        private async Task ReceiveBytesAsync(NetworkStream stream, ReceiveBuffer buffer, CancellationToken cancellationToken)
        {
            for (
                int readBytes = 0, spaceLeft = buffer.Bytes.Length - buffer.Offset; 
                spaceLeft > 0; 
                buffer.Offset += readBytes, spaceLeft -= readBytes, readBytes = 0
            )
            {
                cancellationToken.ThrowIfCancellationRequested();

                if (!stream.DataAvailable)
                {
                    return;
                }

                readBytes = await stream.ReadAsync(buffer.Bytes, buffer.Offset, spaceLeft, cancellationToken).ConfigureAwait(false);
            }
        }

        private async Task ReceiveBytesAsync(NetworkStream stream, byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            for (int readBytes = 0; count > 0; offset += readBytes, count -= readBytes, readBytes = 0)
            {
                cancellationToken.ThrowIfCancellationRequested();

                if (stream.DataAvailable)
                {
                    readBytes = await stream.ReadAsync(buffer, offset, count, cancellationToken).ConfigureAwait(false);
                }
                else
                {
                    await Task.Delay(10).ConfigureAwait(false);
                }
            }
        }

        private async Task AuthenticateAsync(NetworkStream stream, CancellationToken cancellationToken)
        {
            // send HELLO
            await stream.WriteAsync(_helloMessage, 0, _helloMessage.Length, cancellationToken).ConfigureAwait(false);

            // receive answer
            var answer = await ReceiveMessageAsync(stream, 2, cancellationToken).ConfigureAwait(false);

            // check version
            if(answer[0] != 5)
            {
                throw new InvalidOperationException("Wrong version");
            }

            // check mthod selected by server
            if(answer[1] == AuthMethodUsernamePassword)
            {
                await AuthenticateWithUsernameAndPasswordAsync(stream, cancellationToken).ConfigureAwait(false);
            }
            else if(answer[1] > 0)
            {
                throw new InvalidOperationException($"Authentication method {answer[1]} is not supported");
            }
        }

        private async Task AuthenticateWithUsernameAndPasswordAsync(NetworkStream stream, CancellationToken cancellationToken)
        {
            var msg = SocksMessageBuilder.GetUserPassAuthMessage(_username, _password);

            // send auth message
            await stream.WriteAsync(msg, 0, msg.Length, cancellationToken).ConfigureAwait(false);

            // receive answer
            var answer = await ReceiveMessageAsync(stream, 2, cancellationToken).ConfigureAwait(false);

            // check status
            if(answer[1] != 0)
            {
                throw new ArgumentException("Wrong username or password");
            }
        }

        private async Task<byte[]> ReceiveMessageAsync(NetworkStream stream, int size, CancellationToken cancellationToken)
        {
            var buffer = new byte[size];

            await ReceiveBytesAsync(stream, buffer, 0, size, cancellationToken).ConfigureAwait(false);

            return buffer;
        }
    }
}
