﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using sh2ezo.utils.Net.Abstractions;

namespace sh2ezo.utils.Net.Socks5

{
    public partial class Socks5ConnectionFactory : IConnectionFactory
    {
        private string username;
        private string password;
        public TimeSpan Timeout { get; set; }
        private IPEndPoint proxyAddress;

        private const int AuthMethodNotRequired = 0;
        private const int AuthMethodUsernamePassword = 2;

        public Socks5ConnectionFactory(string username, string password, IPEndPoint proxyAddress)
        {
            this.username = username;
            this.password = password;
            this.proxyAddress = proxyAddress;

            Timeout = new TimeSpan(0, 0, 30);
        }

        public NetworkStream Connect(EndPoint endPoint)
        {
            var socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(proxyAddress);
            var stream = new NetworkStream(socket, true);

            try
            {
                // authenticate
                Authenticate(stream);

                SendMessage(stream, SocksMessageBuilder.GetConnectMessage(endPoint));

                var answer = ConvertBytesToSocksReply(ReceiveMessage(stream));

                if (answer.ReplyType != SocksReplyType.Success)
                {
                    throw new SocksNotConnectedException(answer.ReplyType);
                }

                return stream;
            }
            catch
            {
                stream.Close();
                stream.Dispose();
                throw;
            }
        }

        private void Authenticate(NetworkStream stream)
        {
            // отсылаем список доступных методов аутентификации
            SendMessage(stream, new byte[]
            {
                5, // версия протокола
                2, // количество поддерживаемых методов
                AuthMethodNotRequired,
                AuthMethodUsernamePassword
            });

            // принимаем ответ
            var answer = ReceiveMessage(stream, 2);

            // действуем в зависимости от ответа
            if(answer[0] != 5)
            {
                throw new InvalidOperationException("Wrong VER value");
            }

            if(answer[1] == AuthMethodUsernamePassword)
            {
                AuthenticateWithUsernameAndPassword(stream);
            }else if(answer[1] > 0)
            {
                throw new InvalidOperationException($"Authentication method {answer[1]} is not supported");
            }
        }

        private void AuthenticateWithUsernameAndPassword(NetworkStream stream)
        {
            SendMessage(stream, GetUsernameAndPasswordMessageBytes());

            var answer = ReceiveMessage(stream, 2);

            if(answer[1] != 0)
            {
                throw new ArgumentException("Wrong username or password");
            }
        }

        private byte[] GetUsernameAndPasswordMessageBytes()
        {
            var msg = new byte[3 + username.Length + password.Length];
            msg[0] = 1;
            msg[1] = (byte)username.Length;
            msg[2 + username.Length] = (byte)password.Length;

            Encoding.ASCII.GetBytes(username).CopyTo(msg, 2);
            Encoding.ASCII.GetBytes(password).CopyTo(msg, 2 + username.Length + 1);

            return msg;
        }

        private SocksReply ConvertBytesToSocksReply(byte[] replyBytes)
        {
            var reply = new SocksReply
            {
                Version = replyBytes[0],
                ReplyType = (SocksReplyType)replyBytes[1]
            };

            if(reply.ReplyType != SocksReplyType.Success)
            {
                return reply;
            }

            if (replyBytes[3] == 1)
            {
                var ipBytes = new byte[4];
                Array.Copy(replyBytes, 4, ipBytes, 0, 4);
                reply.BoundEndPoint = new IPEndPoint(new IPAddress(ipBytes), (replyBytes[9] << 8) | replyBytes[8]);
            }
            else if(replyBytes[3] == 3)
            {
                var host = Encoding.ASCII.GetString(replyBytes, 5, replyBytes[4]);
                int port = (replyBytes[4 + host.Length + 1] << 8) | replyBytes[4 + host.Length];

                reply.BoundEndPoint = new DnsEndPoint(host, port);
            }else if(replyBytes[3] == 4)
            {
                var ipBytes = new byte[4];
                Array.Copy(replyBytes, 4, ipBytes, 0, 16);
                reply.BoundEndPoint = new IPEndPoint(new IPAddress(ipBytes), (replyBytes[21] << 8) | replyBytes[20]);
            }
            else
            {
                throw new ArgumentException("Wrong ATYP value");
            }

            return reply;
        }

        private void SendMessage(NetworkStream stream, byte[] message)
        {
            stream.Write(message, 0, message.Length);
        }

        private byte[] ReceiveMessage(NetworkStream stream, int messageSize)
        {
            var receiveStart = DateTime.Now;
            byte[] buf = new byte[messageSize];

            for(int i = 0; i < messageSize; i++)
            {
                while(!stream.DataAvailable)
                {
                    if (DateTime.Now - receiveStart > Timeout)
                    {
                        throw new TimeoutException();
                    }

                    Thread.Sleep(10);
                }

                buf[i] = (byte)stream.ReadByte();
            }

            return buf;
        }

        private byte[] ReceiveMessage(NetworkStream stream)
        {
            var buf = new byte[255];

            stream.Read(buf, 0, buf.Length - 1);

            return buf;
        }
    }
}
