﻿namespace sh2ezo.utils.Net.Socks5
{
    internal enum SocksCommand
    {
        Connect = 1,
        Bind = 2,
        UDPAssociate = 3
    }
}
