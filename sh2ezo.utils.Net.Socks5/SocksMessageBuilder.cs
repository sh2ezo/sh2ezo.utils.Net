﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace sh2ezo.utils.Net.Socks5
{
    internal static class SocksMessageBuilder
    {
        public static byte[] GetHelloMessage(byte[] methods)
        {
            var buffer = new byte[2 + methods.Length];

            buffer[0] = 5; // version
            buffer[1] = (byte) methods.Length; // number of supported auth methods

            // copy supported methods
            methods.CopyTo(buffer, 2);

            return buffer;
        }

        public static byte[] GetUserPassAuthMessage(string username, string password)
        {
            var msg = new byte[3 + username.Length + password.Length];

            msg[0] = 1; // version
            msg[1] = (byte)username.Length; // username length
            msg[2 + username.Length] = (byte)password.Length; // password length

            Encoding.ASCII.GetBytes(username).CopyTo(msg, 2); // copy username
            Encoding.ASCII.GetBytes(password).CopyTo(msg, 2 + username.Length + 1); // copy password

            return msg;
        }

        private static byte[] GetPortBytes(int port)
        {
            var portBytes = new byte[2];

            portBytes[0] = (byte)(port / 256);
            portBytes[1] = (byte)(port % 256);

            return portBytes;
        }

        public static byte[] GetCommandMessage(SocksCommand command, IPEndPoint endPoint)
        {
            if (endPoint.AddressFamily == AddressFamily.InterNetwork)
            {
                var ipBytes = endPoint.Address.GetAddressBytes();
                var portBytes = GetPortBytes(endPoint.Port);

                return new byte[]
                {
                    5, // VER
                    (byte)command, // CMD
                    0, // RSV
                    1, // ATYP
                    ipBytes[0], ipBytes[1], ipBytes[2], ipBytes[3], // DST.ADDR
                    portBytes[0], portBytes[1] // DST.PORT
                };
            }
            else if (endPoint.AddressFamily == AddressFamily.InterNetworkV6)
            {
                var portBytes = GetPortBytes(endPoint.Port);

                var msg = new byte[]
                {
                    5, // VER
                    (byte)command, // CMD
                    0, // RSV
                    4, // ATYP
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // DST.ADDR
                    portBytes[0], portBytes[1] // DST.PORT
                };

                endPoint.Address.GetAddressBytes().CopyTo(msg, 4);
            }

            throw new ArgumentException("Not supported address family");
        }

        public static byte[] GetCommandMessage(SocksCommand command, string domain, int port)
        {
            if (domain.Length > 255)
            {
                throw new ArgumentException("Domain name length must be not greater than 255");
            }
            var portBytes = GetPortBytes(port);
            var msg = new byte[7 + domain.Length];

            msg[0] = 5; // VER
            msg[1] = (byte)command; // CMD
            msg[3] = 3; // ATYP
            msg[4] = (byte)domain.Length;
            msg[5 + domain.Length] = portBytes[0];
            msg[5 + domain.Length + 1] = portBytes[1];

            Encoding.ASCII.GetBytes(domain).CopyTo(msg, 5);

            return msg;
        }

        public static byte[] GetCommandMessage(SocksCommand command, EndPoint endPoint)
        {
            if (endPoint.GetType() == typeof(IPEndPoint))
            {
                return GetCommandMessage(command, (IPEndPoint)endPoint);
            }
            else if (endPoint.GetType() == typeof(DnsEndPoint))
            {
                var dnsEndPoint = (DnsEndPoint)endPoint;

                return GetCommandMessage(command, dnsEndPoint.Host, dnsEndPoint.Port);
            }

            throw new ArgumentException("Wrong end point");
        }

        public static byte[] GetConnectMessage(EndPoint endPoint)
        {
            return GetCommandMessage(SocksCommand.Connect, endPoint);
        }
    }
}
