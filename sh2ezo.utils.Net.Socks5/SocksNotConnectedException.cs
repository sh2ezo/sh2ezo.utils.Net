﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace sh2ezo.utils.Net.Socks5
{
    public class SocksNotConnectedException : Exception
    {
        public SocksReplyType SocksReplyType { get; private set; }

        public SocksNotConnectedException(SocksReplyType socksReplyType) : base(socksReplyType.ToString())
        {
            SocksReplyType = socksReplyType;
        }
    }
}
