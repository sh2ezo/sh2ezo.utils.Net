﻿using System.Net;

namespace sh2ezo.utils.Net.Socks5
{
    internal struct SocksReply
    {
        public int Version { get; set; }
        public SocksReplyType ReplyType { get; set; }
        public EndPoint BoundEndPoint { get; set; }
    }
}
