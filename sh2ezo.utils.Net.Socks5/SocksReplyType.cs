﻿namespace sh2ezo.utils.Net.Socks5
{
    public enum SocksReplyType
    {
        Success = 0,
        GeneralSocksServerFailure = 1,
        ConnectionNotAllowedByRuleset = 2,
        NetworkUnreachable = 3,
        HostUnreachable = 4,
        ConnectionRefused = 5,
        TTLExpired = 6,
        CommandNotSupported = 7,
        AddressTypeNotSupported = 8
    }
}
