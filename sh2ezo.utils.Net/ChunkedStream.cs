﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Runtime.CompilerServices;

namespace sh2ezo.utils.Net
{
    class ChunkedStream : Stream
    {
        private Stream _stream;
        private CustomReader _reader;
        private long _chunkBytesLeft = 0;
        private bool _canRead = true;

        public ChunkedStream(Stream stream)
        {
            _stream = stream;
            _reader = new CustomReader(_stream, Encoding.ASCII);
        }

        #region Stream

        public override bool CanRead => _canRead;

        public override bool CanSeek => false;

        public override bool CanWrite => false;

        public override long Length => throw new NotSupportedException();

        public override long Position { get => throw new NotSupportedException(); set => throw new NotSupportedException(); }

        public override void Flush()
        {
            throw new NotSupportedException();
        }

        public override int ReadTimeout { get => _stream.ReadTimeout; set => _stream.ReadTimeout = value; }

        public override int WriteTimeout { get => _stream.WriteTimeout; set => _stream.WriteTimeout = value; }

        public override bool CanTimeout => _stream.CanTimeout;

        public override int Read(byte[] buffer, int offset, int count)
        {
            return Read(buffer, offset, count, default(CancellationToken));
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        #endregion

        private void ReadCrLf(CancellationToken cancel)
        {
            var bytes = _stream.ReadBytes(2, cancel);

            if (bytes[0] != '\r')
            {
                throw new InvalidOperationException($"No CR was found! Actual char code: 0x{bytes[0]:X2}");
            }

            if (bytes[1] != '\n')
            {
                throw new InvalidOperationException($"No LF was found! Actual char code: 0x{bytes[1]:X2}");
            }
        }

        public bool BeginReadNextChunk(CancellationToken cancel)
        {
            var chunkSizeString = _reader.ReadLine(cancel);
            _chunkBytesLeft = int.Parse(chunkSizeString, System.Globalization.NumberStyles.HexNumber);

            if (_chunkBytesLeft == 0) // if size of another chunk is equal to zero then lock stream
                                      // read last CRLF characters and lock this stream
            {
                ReadCrLf(cancel);
                _canRead = false;
                return false;
            }

            return true;
        }

        private int Read(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            if (!_canRead)
            {
                return 0;
            }

            if (_chunkBytesLeft == 0) // if previous chunk ends up then read another one
            {
                if (!BeginReadNextChunk(cancellationToken))
                {
                    return 0;
                }
            }

            int bytesRead = _stream.Read(buffer, offset, Math.Min(count, (int)(_chunkBytesLeft % int.MaxValue)));
            _chunkBytesLeft -= bytesRead;

            if (_chunkBytesLeft == 0)
            {
                ReadCrLf(cancellationToken);
                BeginReadNextChunk(cancellationToken);
            }

            return bytesRead;
        }

        public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancel)
        {
            return await Task.Run(() => Read(buffer, offset, count, cancel)).ConfigureAwait(false);
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                _stream.Dispose();
            }
        }
    }
}
