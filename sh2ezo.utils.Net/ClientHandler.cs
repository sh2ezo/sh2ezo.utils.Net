﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Threading;
using System.Net;
using System.Net.Security;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using sh2ezo.utils.Net.Abstractions;

namespace sh2ezo.utils.Net
{
    public class ClientHandler : HttpMessageHandler
    {
        public bool AllowAutoRedirect { get; set; } = true;
        public CookieContainer CookieContainer
        {
            get => cookieHandler.CookieContainer;
            set => cookieHandler.CookieContainer = value;
        }
        public bool UseCookies { get; set; } = true;
        public int MaxAutomaticRedirections { get; set; } = 20;
        public RemoteCertificateValidationCallback CertificateValidationCallback
        {
            get => connectionsHandler.CertificateValidationCallback;
            set => connectionsHandler.CertificateValidationCallback = value;
        }
        private CancellationTokenSource _cts = new CancellationTokenSource();

        private CookieHandler cookieHandler; 
        private ConnectionsHandler connectionsHandler;
        
        public ClientHandler(IConnectionFactory factory)
        {
            cookieHandler = new CookieHandler();
            connectionsHandler = new ConnectionsHandler(factory);
        }

        private void ProcessAllowedEncodings(HttpRequestMessage request)
        {
            if(request.Headers.AcceptEncoding == null || string.IsNullOrEmpty(request.Headers.AcceptEncoding.ToString()))
            {
                request.Headers.Add("Accept-Encoding", "gzip, deflate");
            }
        }

        private byte[] SerializeRequest(HttpRequestMessage request, byte[] contentBytes)
        {
            var builder = new StringBuilder();
            request.SerializeWithoutContent(builder);
            
            if(request.Content != null)
            {
                request.WriteContentHeaders(contentBytes.Length, builder);
            }

            builder.Append("\r\n");
            var requestBytes = Encoding.UTF8.GetBytes(builder.ToString());
            return requestBytes;
        }

        private async Task<HttpResponseMessage> SendInternalAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            byte[] contentBytes;

            if (request.Content != null)
            {
                contentBytes = await request.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
            }
            else
            {
                contentBytes = null;
            }

            ProcessAllowedEncodings(request);

            var nextUri = request.RequestUri;
            HttpResponseMessage response = null;

            for (int redirectionNumber = -1; redirectionNumber < MaxAutomaticRedirections; redirectionNumber += 1)
            {
                using (var _request = request.CloneWithoutContent())
                {
                    _request.RequestUri = nextUri;
                    _request.Content = request.Content;

                    using (var scopedStream = connectionsHandler.Connect(request.RequestUri))
                    {
                        try
                        {
                            if (UseCookies)
                            {
                                cookieHandler.LoadCookies(_request);
                            }

                            var requestBytes = SerializeRequest(_request, contentBytes);
                            await scopedStream.WriteAsync(requestBytes, 0, requestBytes.Length, cancellationToken).ConfigureAwait(false);

                            if (request.Content != null)
                            {
                                await scopedStream.WriteAsync(contentBytes, 0, contentBytes.Length, cancellationToken).ConfigureAwait(false);
                            }

                            using (var reader = new ResponseReader(scopedStream))
                            {
                                try
                                {
                                    response = await reader.ReadAsync(cancellationToken).ConfigureAwait(false);
                                }
                                catch (WrongResponseException)
                                {
                                    throw;
                                }
                            }

                            response.RequestMessage = _request;

                            if (UseCookies)
                            {
                                cookieHandler.SaveCookies(response);
                            }

                            if (AllowAutoRedirect && response.Headers.Location != null)
                            {
                                nextUri = response.Headers.Location;
                            }
                            else
                            {
                                break;
                            }
                        }
                        finally
                        {
                            if ((_request.Headers.ConnectionClose.HasValue && _request.Headers.ConnectionClose.Value) ||
                                (response != null && response.Headers.ConnectionClose.HasValue && response.Headers.ConnectionClose.Value))
                            {
                                scopedStream.CloseOnDispose();
                            }
                        }
                    }
                }

            }

            return response;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            using(var cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, _cts.Token))
            {
                return await SendInternalAsync(request, cts.Token).ConfigureAwait(false);
            }   
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!disposing)
                {
                    return;
                }

                _cts.Cancel();
                _cts.Dispose();
            }
            finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
