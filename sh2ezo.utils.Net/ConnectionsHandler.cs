﻿using System;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Concurrent;
using System.Threading;
using sh2ezo.utils.Net.Abstractions;

namespace sh2ezo.utils.Net
{
    class ConnectionsHandler
    {
        private struct StreamProperties
        {
            public NetworkStream Stream { get; set; }
            public EndPoint EndPoint { get; set; }
            public DateTime ExpireTime { get; set; }
        }

        private IConnectionFactory _factory;
        private ConcurrentDictionary<EndPoint, ConcurrentQueue<ScopedStream>> _pointsStreams = new ConcurrentDictionary<EndPoint, ConcurrentQueue<ScopedStream>>();
        private ConcurrentDictionary<ScopedStream, StreamProperties> _streamProperties = new ConcurrentDictionary<ScopedStream, StreamProperties>();
        public RemoteCertificateValidationCallback CertificateValidationCallback { get; set; }
        private SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        public ConnectionsHandler(IConnectionFactory factory)
        {
            _factory = factory;
        }

        public ScopedStream Connect(Uri targetUri)
        {
            bool secure;

            if(targetUri.Scheme == "http")
            {
                secure = false;
            }
            else if(targetUri.Scheme == "https")
            {
                secure = true;
            }
            else
            {
                throw new WebException("Not supported scheme!");
            }

            if (targetUri.HostNameType == UriHostNameType.Dns)
            {
                return GetConnectionStream(new DnsEndPoint(targetUri.Host, targetUri.Port), secure);
            }
            else if (targetUri.HostNameType == UriHostNameType.IPv4 || targetUri.HostNameType == UriHostNameType.IPv6)
            {
                return GetConnectionStream(new IPEndPoint(IPAddress.Parse(targetUri.Host), targetUri.Port), secure);
            }

            throw new Exception("Wrong host name");
        }

        public void ReleaseStream(EndPoint endPoint, ScopedStream stream)
        {
            if (!_streamProperties.ContainsKey(stream))
            {
                throw new InvalidOperationException("no properties for stream");
            }

            _pointsStreams[endPoint].Enqueue(stream);
        }

        private bool TryGetExistingStream(EndPoint endPoint, out ScopedStream stream)
        {
            stream = null;

            if (!_pointsStreams.TryGetValue(endPoint, out var pointStreams))
            {
                return false;
            }

            if (!pointStreams.TryDequeue(out stream))
            {
                return false;
            }

            var streamProperties = _streamProperties[stream];

            if (streamProperties.ExpireTime > DateTime.Now)
            {
                return true;
            }
            else
            {
                Remove(stream);
                return false;
            }
        }

        private Stream GetTransportStream(Stream stream, EndPoint endPoint, bool secure)
        {
            if (secure)
            {
                var sslStream = new SslStream(
                    stream,
                    false,
                    CertificateValidationCallback
                );

                try
                {
                    sslStream.AuthenticateAsClient(endPoint.GetHost(), null, SslProtocols.Tls12, false);
                }
                catch (AuthenticationException exc)
                {
                    Console.WriteLine("Authentication exception: {0}", exc.Message);
                    stream.Dispose();
                    throw;
                }

                stream = sslStream;
            }

            return stream;
        }

        private void AddStream(EndPoint endPoint, ScopedStream s)
        {
            if (!_pointsStreams.TryGetValue(endPoint, out var streams))
            {
                _pointsStreams.TryAdd(endPoint, new ConcurrentQueue<ScopedStream>());
            }
        }

        private ScopedStream GetConnectionStream(EndPoint endPoint, bool secure)
        {
            if (TryGetExistingStream(endPoint, out ScopedStream existing))
            {
                return existing;
            }
            else
            {
                var stream = _factory.Connect(endPoint);
                var transport = GetTransportStream(stream, endPoint, secure);
                var connection = new ScopedStream(this, transport, endPoint);

                AddStream(endPoint, connection);

                _streamProperties[connection] = new StreamProperties
                {
                    Stream = stream,
                    EndPoint = endPoint,
                    ExpireTime = DateTime.Now.AddMilliseconds(60000)
                };

                return connection;
            }
        }

        public void Remove(ScopedStream stream)
        {
            if(_streamProperties.TryGetValue(stream, out var properties))
            {
                properties.Stream.Close();
                properties.Stream.Dispose();
                _streamProperties.TryRemove(stream, out properties);
            }
        }
    }
}
