﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

namespace sh2ezo.utils.Net
{
    class CookieHandler
    {
        public CookieContainer CookieContainer { get; set; } = new CookieContainer();

        public CookieHandler()
        {
        }

        public void SaveCookies(HttpResponseMessage response)
        {
            if (response.Headers.Contains("set-cookie"))
            {
                foreach (var cookieHeader in response.Headers.GetValues("set-cookie"))
                {
                    CookieContainer.SetCookies(response.RequestMessage.RequestUri, cookieHeader);
                }
            }
        }

        public void LoadCookies(HttpRequestMessage request)
        {
            var cookies = CookieContainer.GetCookies(request.RequestUri);

            if(cookies.Count > 0)
            {
                var cookieString = string.Join("; ",
                    cookies
                    .OfType<Cookie>()
                    .Select(c => $"{c.Name}={c.Value}")
                );

                request.Headers.Add("Cookie", cookieString);
            }
        }
    }
}
