﻿using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace sh2ezo.utils.Net
{
    internal class CustomReader
    {
        private Stream _stream;
        private Encoding _encoding;
        private const byte CR = 13;
        private const byte LF = 10;

        public CustomReader(Stream stream, Encoding encoding)
        {
            _stream = stream;
            _encoding = encoding;
        }

        public string ReadLine(CancellationToken cancel)
        {
            var bytes = new List<byte>();
            bool cr = false;

            while (true)
            {
                var buf = _stream.ReadBytes(1, cancel);
                var b = buf[0];

                if (b == CR)
                {
                    cr = true;
                }
                else if (cr)
                {
                    if (b == LF)
                    {
                        return _encoding.GetString(bytes.ToArray());
                    }
                    else
                    {
                        cr = false;
                        bytes.Add(CR);
                        bytes.Add(b);
                    }
                }
                else
                {
                    bytes.Add(b);
                }
            }
        }

        public Task<string> ReadLineAsync(CancellationToken cancel)
        {
            return Task.Run(() => ReadLine());
        }

        public string ReadLine()
        {
            return ReadLine(default(CancellationToken));
        }
    }
}
