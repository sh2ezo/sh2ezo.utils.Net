﻿using System;
using System.Net;
using System.Net.Sockets;

namespace sh2ezo.utils.Net
{
    static class EndPointExtensions
    {
        public static string GetHost(this EndPoint endPoint)
        {
            if(endPoint.GetType() == typeof(IPEndPoint))
            {
                return ((IPEndPoint)endPoint).Address.ToString();
            }else if(endPoint.GetType() == typeof(DnsEndPoint))
            {
                return ((DnsEndPoint)endPoint).Host;
            }
            else
            {
                throw new WebException("Unsupported type of end point!");
            }
        }

        public static int GetPort(this EndPoint endPoint)
        {
            if(endPoint is IPEndPoint ip)
            {
                return ip.Port;
            }else if(endPoint is DnsEndPoint dns)
            {
                return dns.Port;
            }
            else
            {
                throw new WebException("Unsupported type of end point!");
            }
        }
    }
}
