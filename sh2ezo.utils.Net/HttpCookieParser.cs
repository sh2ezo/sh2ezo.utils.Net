﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace sh2ezo.utils.Net
{
    static class HttpCookieParser
    {
        public static Cookie Parse(string cookieString)
        {
            var parts = cookieString.Split(';');
            var cookieName = parts[0].Substring(0, parts[0].IndexOf('='));
            var cookieValue = parts[0].Substring(cookieName.Length + 1);
            var cookie = new Cookie();

            cookie.Name = cookieName;
            cookie.Value = cookieValue;
            

            foreach(var _part in parts.Skip(1))
            {
                var part = _part.Trim();

                if(part.StartsWith("Expires", StringComparison.OrdinalIgnoreCase))
                {
                    cookie.Expires = RFC1123DateTimeParser.Parse(part.Split('=')[1]);
                }else if(part.StartsWith("Domain", StringComparison.OrdinalIgnoreCase))
                {
                    var domain = part.Split('=')[1];

                    cookie.Domain = domain;

                }else if(part.StartsWith("Path", StringComparison.OrdinalIgnoreCase))
                {
                    cookie.Path = part.Split('=')[1];
                }else if(part.Equals("Secure", StringComparison.OrdinalIgnoreCase))
                {
                    cookie.Secure = true;
                }else if(part.Equals("HttpOnly", StringComparison.OrdinalIgnoreCase))
                {
                    cookie.HttpOnly = true;
                }
            }

            return cookie;
        }
    }
}
