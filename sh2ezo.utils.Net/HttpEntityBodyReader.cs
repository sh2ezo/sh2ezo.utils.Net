﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.IO.Compression;
using System.Threading;

namespace sh2ezo.utils.Net
{
    class HttpEntityBodyReader
    {
        Stream _stream;
        int contentLength;

        public HttpEntityBodyReader(IDictionary<string, IEnumerable<string>> headers, Stream stream)
        {
            if(headers.TryGetValue("transfer-encoding", out var transEncValue))
            {
                if(transEncValue.First() == "chunked")
                {
                    _stream = new ChunkedStream(stream);
                }
                else
                {
                    throw new WebException($"Unsupported transfer encoding: {transEncValue.First()}");
                }
            }
            else
            {
                _stream = stream;
            }

            if (headers.TryGetValue("content-length", out var contLenValue))
            {
                contentLength = int.Parse(contLenValue.First());
            }
            else
            {
                contentLength = -1;
            }

            if (headers.TryGetValue("content-encoding", out var contEncValue))
            {
                contentLength = -1;

                if (contEncValue.First() == "gzip")
                {
                    _stream = new GZipStream(_stream, CompressionMode.Decompress);
                }
                else if (contEncValue.First() == "deflate")
                {
                    _stream = new DeflateStream(_stream, CompressionMode.Decompress);
                }
                else
                {
                    throw new WebException($"Unsupported content encoding: {contEncValue.First()}");
                }
            }
        }

        public async Task<byte[]> ReadAsync(CancellationToken cancellationToken)
        {
            if(contentLength < 0)
            {
                return await _stream.ReadBytesAsync(cancellationToken).ConfigureAwait(false);
            }else
            {
                return await _stream.ReadBytesAsync(contentLength, cancellationToken).ConfigureAwait(false);
            }
        }
    }
}
