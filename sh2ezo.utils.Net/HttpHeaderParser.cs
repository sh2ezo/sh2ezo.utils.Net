﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace sh2ezo.utils.Net
{
    class HttpHeaderParser
    {
        private readonly static HashSet<string> DateHeaders = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "date",
            "expires",
            "last-modified",
            "retry-after"
        };

        private readonly static HashSet<string> WeekDays = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
        };

        private const string Charset = "charset";

        private static IEnumerable<string> ParseCookieHeaderValue(string headerValue)
        {
            var cookies = new List<string>();

            int i = 0;
            int j = headerValue.IndexOf(',');

            for (; j > i; j = headerValue.IndexOf(',', j + 1))
            {
                if (WeekDays.Contains(headerValue.Substring(j - 3, 3)))
                {
                    continue;
                }
                else
                {
                    cookies.Add(headerValue.Substring(i, j - i));
                    i = j + 1;
                }
            }

            cookies.Add(headerValue.Substring(i));

            return cookies;
        }

        private static string RemoveQuotesFromCharset(string headerValue)
        {
            // смотрим, есть ли вообще обозначение кодировки
            var charsetStart = headerValue.IndexOf(Charset);

            if(charsetStart < 0)
            {
                return headerValue;
            }

            // если обозначение есть, перескакиваем через название параметра, чтобы не обрабатывать его
            charsetStart += Charset.Length;

            var builder = new StringBuilder();

            // в новую строку пихаем все, что до кодировки
            builder.Append(headerValue.Substring(0, charsetStart));

            for(int i = charsetStart, endI = headerValue.Length; i < endI; i += 1)
            {
                var c = headerValue[i];

                if (c == ';') // если начался новый параметр
                {
                    // положим в результат оставшиеся символы и выйдем из цикла
                    builder.Append(headerValue.Substring(i));
                    break;
                }
                else if (c != '"') // если не кавычка
                {
                    builder.Append(c); // положим в результирующую строку
                }                
            }

            return builder.ToString();
        }

        public static KeyValuePair<string, IEnumerable<string>> ParseHeader(string headerString)
        {
            var headerName = headerString.Substring(0, headerString.IndexOf(':'));
            var headerValue = headerString.Substring(headerName.Length + 1).Trim();

            if(DateHeaders.Contains(headerName))
            {
                return new KeyValuePair<string, IEnumerable<string>>(headerName, new[] { headerValue });
            }
            else if(headerName.Equals("set-cookie", StringComparison.OrdinalIgnoreCase))
            {
                return new KeyValuePair<string, IEnumerable<string>>(headerName, ParseCookieHeaderValue(headerValue));
            }
            else if(headerName.Equals("content-type", StringComparison.OrdinalIgnoreCase))
            {
                return new KeyValuePair<string, IEnumerable<string>>(headerName, new[] { RemoveQuotesFromCharset(headerValue) });
            }
            else
            {
                return new KeyValuePair<string, IEnumerable<string>>(headerName, headerValue.Split(','));
            }
        }
    }
}
