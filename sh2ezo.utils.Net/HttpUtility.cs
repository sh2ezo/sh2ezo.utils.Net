﻿using System;
using System.Text;

namespace sh2ezo.utils.Net
{
    public class HttpUtility
    {
        /// <summary>
        /// Encodes value with UTF8
        /// </summary>
        public static string UrlEncode(string value)
        {
            return UrlEncode(value, Encoding.UTF8);
        }

        public static string UrlEncode(string value, Encoding encoding)
        {
            var builder = new StringBuilder();
            var buf = new byte[8];

            for(int i = 0, eI = value.Length; i < eI; i += 1)
            {
                var c = value[i];

                if(c == ' ')
                {
                    builder.Append('+');
                }else if((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                {
                    builder.Append(c);
                }
                else
                {
                    int bytesCount = encoding.GetBytes(value, i, 1, buf, 0);

                    for(int b = 0; b < bytesCount; b += 1)
                    {
                        builder.Append("%").Append(Convert.ToString(buf[b], 16));
                    }
                }
            }

            return builder.ToString();
        }
    }
}
