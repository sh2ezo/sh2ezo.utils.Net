﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using sh2ezo.utils.Net.Abstractions;

namespace sh2ezo.utils.Net
{
    public class NoProxyConnectionFactory : IConnectionFactory
    {
        public NetworkStream Connect(EndPoint endPoint)
        {
            var socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            return new NetworkStream(socket, true);
        }
    }
}
