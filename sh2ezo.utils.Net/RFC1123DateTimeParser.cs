﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;

namespace sh2ezo.utils.Net
{
    static class RFC1123DateTimeParser
    {
        private static readonly Regex rxRFC1123DateTime = new Regex(@"(\d{2})[-\s](\w{3})[-\s](\d{2,4}) (\d{2}):(\d{2}):(\d{2})", RegexOptions.Compiled);
        private static readonly CultureInfo AmericanCulture = new CultureInfo("en-US");

        public static DateTime Parse(string dateString)
        {
            var dateMatch = rxRFC1123DateTime.Match(dateString);
            var groups = dateMatch.Groups;

            var dateFormatted = $"{groups[1].Value} {groups[2].Value} {groups[3].Value} {groups[4].Value}:{groups[5].Value}:{groups[6].Value}";            

            if(groups[3].Value.Length == 2)
            {
                return DateTime.ParseExact(
                    dateFormatted,
                    "dd MMM yy HH:mm:ss",
                    AmericanCulture
                );
            }
            else
            {
                return DateTime.ParseExact(
                    dateFormatted,
                    "dd MMM yyyy HH:mm:ss",
                    AmericanCulture
                );
            }
        }
    }
}
