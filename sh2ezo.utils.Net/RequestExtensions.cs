﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;

namespace sh2ezo.utils.Net
{
    static class RequestExtensions
    {
        public static HttpRequestMessage CloneWithoutContent(this HttpRequestMessage message)
        {
            var result = new HttpRequestMessage();

            foreach(var header in message.Headers)
            {
                result.Headers.Add(header.Key, header.Value);
            }

            result.Method = message.Method;
            
            foreach(var property in message.Properties)
            {
                result.Properties[property.Key] = property.Value;
            }

            result.RequestUri = message.RequestUri;
            result.Version = message.Version;

            return result;
        }

        private static string GetRequestLine(this HttpRequestMessage request)
        {
            var query = $"{request.RequestUri.PathAndQuery}";

            return $"{request.Method.Method} {query} HTTP/{request.Version}\r\n";
        }

        public static void SerializeWithoutContent(this HttpRequestMessage request, StringBuilder builder)
        {
            builder.Append(request.GetRequestLine());

            request.Headers.Host = request.RequestUri.Host;

            foreach (var header in request.Headers)
            {
                if(header.Key.Equals("User-Agent", StringComparison.OrdinalIgnoreCase))
                {
                    var value = string.Join(" ", header.Value);
                    builder.Append($"{header.Key}: {value}\r\n");
                }
                else
                {
                    foreach (var value in header.Value)
                    {
                        builder.Append($"{header.Key}: {value}\r\n");
                    }
                }
            }
        }

        public static void WriteContentHeaders(this HttpRequestMessage request, int contentLength, StringBuilder builder)
        {
            foreach (var header in request.Content.Headers)
            {
                foreach (var value in header.Value)
                {
                    builder.Append($"{header.Key}: {value}\r\n");
                }
            }

            if (!request.Content.Headers.Contains("content-length"))
            {
                builder.Append($"Content-Length: {contentLength}\r\n");
            }
        }
    }
}
