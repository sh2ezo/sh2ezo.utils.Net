﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sh2ezo.utils.Net
{
    class ResponseReader : IDisposable
    {
        private static HashSet<string> entityHeaders = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "allow",
            "content-encoding",
            "content-language",
            "content-length",
            "content-location",
            "content-md5",
            "content-range",
            "content-type",
            "expires",
            "last-modified"
        };

        Stream _innerStream;
        CustomReader _reader;

        public ResponseReader(Stream innerStream)
        {
            _innerStream = innerStream;
            _reader = new CustomReader(_innerStream, System.Text.Encoding.ASCII);
        }

        private async Task ProcessStatusLineAsync(HttpResponseMessage message, CancellationToken cancellationToken)
        {
            var statusLine = await _reader.ReadLineAsync(cancellationToken).ConfigureAwait(false);

            if (!statusLine.StartsWith("HTTP", StringComparison.OrdinalIgnoreCase))
            {
                var wholeResponse = await _innerStream.ReadBytesAsync(cancellationToken).ConfigureAwait(false); // read all bytes
                var wholeResponseString = statusLine + "\r\n" + Encoding.UTF8.GetString(wholeResponse);
                var exception = new HttpRequestException($"Received a wrong status line: {statusLine}");
                exception.Data.Add("Response", wholeResponseString);
                System.Diagnostics.Debug.WriteLine(wholeResponseString);
                throw exception;
            }

            int httpVersionBegin = statusLine.IndexOf('/') + 1;
            int httpVersionEnd = statusLine.IndexOf(' ');
            message.Version = Version.Parse(statusLine.Substring(httpVersionBegin, httpVersionEnd - httpVersionBegin));

            int httpStatusEnd = statusLine.IndexOf(' ', httpVersionEnd + 1);
            var httpStatusString = statusLine.Substring(httpVersionEnd + 1, httpStatusEnd - (httpVersionEnd + 1));
            int httpStatus = int.Parse(httpStatusString);

            message.StatusCode = (HttpStatusCode)httpStatus;
        }

        private async Task<Dictionary<string, IEnumerable<string>>> ReadHeadersAsync(CancellationToken cancel)
        {
            var headers = new Dictionary<string, IEnumerable<string>>(StringComparer.OrdinalIgnoreCase);

            while (true)
            {
                var headerString = await _reader.ReadLineAsync(cancel).ConfigureAwait(false);

                if (string.IsNullOrEmpty(headerString))
                {
                    break;
                }

                var header = HttpHeaderParser.ParseHeader(headerString);

                if (!headers.ContainsKey(header.Key))
                {
                    headers.Add(header.Key, header.Value);
                }
                else
                {
                    headers[header.Key] = headers[header.Key].Concat(header.Value);
                }
            }

            return headers;
        }

        public async Task<HttpResponseMessage> ReadAsync(CancellationToken cancel)
        {
            var message = new HttpResponseMessage();
            await ProcessStatusLineAsync(message, cancel).ConfigureAwait(false);
            var headers = await ReadHeadersAsync(cancel).ConfigureAwait(false);

            if (headers.ContainsKey("content-type"))
            {
                var contentBytes = await new HttpEntityBodyReader(headers, _innerStream).ReadAsync(cancel).ConfigureAwait(false);
                message.Content = new StreamContent(new MemoryStream(contentBytes));
            }

            foreach (var header in headers)
            {
                if (entityHeaders.Contains(header.Key))
                {
                    message.Content.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }
                else
                {
                    message.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }
            }

            return message;
        }

        public void Dispose()
        {
            //_reader.Dispose();
        }
    }
}
