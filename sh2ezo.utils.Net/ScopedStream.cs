﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace sh2ezo.utils.Net
{
    class ScopedStream : Stream
    {
        private bool _disposed;
        private ConnectionsHandler _handler;
        private Stream _stream;
        private EndPoint _endPoint;
        private bool _closeOnDispose;

        #region Stream wrapper

        public override bool CanRead => _stream.CanRead;

        public override bool CanSeek => _stream.CanSeek;

        public override bool CanWrite => _stream.CanWrite;

        public override long Length => _stream.Length;

        public override long Position { get => _stream.Position; set => _stream.Position = value; }

        public override void Flush()
        {
            _stream.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _stream.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _stream.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _stream.Write(buffer, offset, count);
        }

        public override int ReadTimeout { get => _stream.ReadTimeout; set => _stream.ReadTimeout = value; }

        public override int WriteTimeout { get => _stream.WriteTimeout; set => _stream.WriteTimeout = value; }

        public override bool CanTimeout => _stream.CanTimeout;

        #endregion

        public ScopedStream(ConnectionsHandler handler, Stream stream, EndPoint endPoint)
        {
            _handler = handler;
            _stream = stream;
            _endPoint = endPoint;
        }

        public void CloseOnDispose()
        {
            _closeOnDispose = true;
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }

            if (_closeOnDispose)
            {
                _disposed = true;
                _stream.Close();
                _handler.Remove(this);
            }
            else
            {
                _handler.ReleaseStream(_endPoint, this);
            }
        }
    }
}
