﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace sh2ezo.utils.Net
{
    static class StreamExtensions
    {
        private const int ReadTimeoutMs = 100;

        public static Task<byte[]> ReadBytesAsync(this Stream stream, int count, CancellationToken cancellationToken)
        {
            return Task.Run(() => ReadBytes(stream, count, cancellationToken));
        }

        public static byte[] ReadBytes(this Stream stream, int count, CancellationToken cancellationToken)
        {
            int oldTimeout = 0;

            if (stream.CanTimeout)
            {
                oldTimeout = stream.ReadTimeout;
                stream.ReadTimeout = ReadTimeoutMs;
            }

            try
            {
                var bytes = new byte[count];

                for (int i = 0; i < count;)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    int read = stream.Read(bytes, i, count - i);

                    i += read;
                }

                return bytes;
            }
            finally
            {
                if(stream.CanTimeout)
                {
                    stream.ReadTimeout = oldTimeout;
                }
            }
        }

        public static Task<byte[]> ReadBytesAsync(this Stream stream, CancellationToken cancellationToken)
        {
            return Task.Run(() => ReadBytes(stream, cancellationToken));
        }

        public static byte[] ReadBytes(this Stream stream, CancellationToken cancellationToken)
        {
            int oldTimeout = 0;

            if (stream.CanTimeout)
            {
                oldTimeout = stream.ReadTimeout;
                stream.ReadTimeout = ReadTimeoutMs;
            }

            try
            {
                var bytes = new byte[2048];
                int count = 0;

                for (int i = 0, read; ;)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    read = stream.Read(bytes, i, 2048);
                    count += read;

                    if (read == 0)
                    {
                        break;
                    }

                    i += read;

                    Array.Resize(ref bytes, i + 2048);
                }

                Array.Resize(ref bytes, count);

                return bytes;
            }
            finally
            {
                if (stream.CanTimeout)
                {
                    stream.ReadTimeout = oldTimeout;
                }
            }
        }
    }
}
