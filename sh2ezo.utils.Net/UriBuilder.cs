﻿using System;
using System.Collections.Generic;
using System.Linq;
using UriBuilderOld = System.UriBuilder;

namespace sh2ezo.utils.Net
{
    public class UriBuilder
    {
        public string Scheme { get; set; }
        public Dictionary<string, string> Query { get; set; }
        public int? Port { get; set; }
        public string Path { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Fragment { get; set; }
        public string Host { get; set; }

        public Uri Uri
        {
            get
            {
                var pairs = Query.Select(kv => kv.Key + "=" + HttpUtility.UrlEncode(kv.Value));
                var query = string.Join("&", pairs);
                var path = Path;

                if (path.Contains("?"))
                {
                    var parts = path.Split('?');
                    path = parts[0];

                    if (!string.IsNullOrEmpty(query))
                    {
                        query += "&" + parts[1];
                    }
                    else
                    {
                        query = parts[1];
                    }
                }

                var builder = new UriBuilderOld
                {
                    Fragment = Fragment,
                    Host = Host,
                    Password = Password,
                    Path = path,
                    Scheme = Scheme,
                    UserName = UserName,
                    Query = query
                };

                if (Port.HasValue)
                {
                    builder.Port = Port.Value;
                }

                return builder.Uri;
            }
        }
    }
}
